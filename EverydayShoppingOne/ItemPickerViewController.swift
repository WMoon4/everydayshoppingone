//
//  ItemPickerViewController.swift
//  EverydayShoppingOne
//
//  Created by V.K. on 2/27/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ItemPickerViewController: UIViewController {
    
    let vegList = ["Tomatos", "Cucumbers", "Onions", "Garlic", "Dill", "Sweet paprika"]
    
    @IBOutlet weak var itemPickerView: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        itemPickerView.delegate = self
        itemPickerView.dataSource = self
    }

}

extension ItemPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vegList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return vegList[row]
    }
}
