//
//  ShoppingListsData.swift
//  EverydayShoppingOne
//
//  Created by V.K. on 2/26/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import Foundation

enum Category: String {
    case groceries = "Groceries"
    case homestuff = "Home stuff"
    case cosmetics = "Cosmetics"
    case drugs = "Drugs"
}

class ShoppingItem {
    let name: String
    var restriction: String = "."
    
    init(from item: Item) {
        self.name = item.name
    }
}

class Item {
    let name: String
    let category: Category
    
    init(named: String, to: Category) {
        self.name = named
        self.category = to
    }
}

class List {
    var items: [ShoppingItem] = []
    let category: Category
    var special: Bool = false
    let name: String = ""
    
    init(regular to: Category) {
        self.category = to
    }
    
}
