//
//  ViewController.swift
//  EverydayShoppingOne
//
//  Created by V.K. on 2/26/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var lists: [List] = []
    var itemCollection: [Item] = []
    
    let vegList = ["* * *", "Tomatos", "Cucumbers", "Onions", "Garlic", "Dill", "Sweet paprika"]
    var isPresentPickerView = false
    var latestSelectedItemToAdd: String?
    
    @IBOutlet weak var shoppingListTableView: UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemCollection += [
            Item(named: "Eggs", to: .groceries),
            Item(named: "Butter", to: .groceries),
            Item(named: "Bread", to: .groceries),
            Item(named: "Coffee", to: .groceries),
            Item(named: "Tea", to: .groceries),
            Item(named: "Apples", to: .groceries),
            Item(named: "Beer", to: .groceries),
            Item(named: "Flour", to: .groceries),
            Item(named: "Vanilla sugar", to: .groceries)
        ]
        
        lists += [List(regular: .groceries)]
        for i in 0..<7 {
            lists[0].items += [ShoppingItem(from: itemCollection[i])]
        }
        //lists[0].special = true
        
        shoppingListTableView.delegate = self
        shoppingListTableView.dataSource = self
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists[0].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as? ShoppingListTableViewCell
        else {
            assertionFailure("Can't load reusable string")
            return UITableViewCell()
        }
        cell.textLabel?.text = lists[0].items[indexPath.row].name
        cell.detailTextLabel?.text = (lists[0].special) ? lists[0].items[indexPath.row].restriction : ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

//        let addItemView: AddShopingListItemView = .fromNib()
//        addItemView.addShoppingItem = { [weak self] name in
//            self?.lists[0].items += [ShoppingItem(from: Item(named: name, to: .groceries))]
//            self?.shoppingListTableView.reloadSections([0], with: .none)
//        }
//        addItemView.backgroundColor = .lightGray
//        return addItemView
        if isPresentPickerView {
            let pickerView: AddItemPickerView = .fromNib()
            pickerView.backgroundColor = .cyan
            pickerView.delegate = self
            pickerView.dataSource = self
            return pickerView
        } else {
            let addItemView: AddShopingListItemView = .fromNib()
            addItemView.addShoppingItem = { [weak self] _ in
                self?.isPresentPickerView = true
                self?.shoppingListTableView.reloadSections([0], with: .none)
                           
            }
            addItemView.backgroundColor = .lightGray
            return addItemView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 100
        if isPresentPickerView {
            return 216
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected table row: \(indexPath.row)")
        if isPresentPickerView {
            if let name = latestSelectedItemToAdd {
                if name != "* * *" {
                    self.lists[0].items += [ShoppingItem(from: Item(named: name, to: .groceries))]
                }
                latestSelectedItemToAdd = nil
            }
            isPresentPickerView = false
            self.shoppingListTableView.reloadSections([0], with: .none)
        }
        
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let rowAction = UIContextualAction(style: .destructive, title: "DELETE") { [weak self] ( _, _, _) in
            self?.lists[0].items.remove(at: indexPath.row)
            self?.shoppingListTableView.reloadSections([0], with: .none)
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [rowAction])
        return swipeActions
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let rowAction = UIContextualAction(style: .normal, title: "add note") { [weak self] ( _, _, _) in
            //
            self?.shoppingListTableView.reloadSections([0], with: .none)
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [rowAction])
        return swipeActions
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vegList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return vegList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        latestSelectedItemToAdd = vegList[row]
        print("selected: \(vegList[row])")

    }
}
